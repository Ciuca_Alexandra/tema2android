package com.example.tema22.activities

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.tema22.R
import com.example.tema22.VolleyConfigSingleton
import com.example.tema22.adapters.MyAdapter
import com.example.tema22.fragments.FirstFragment
import com.example.tema22.interfaces.OnUserItemClick
import com.example.tema22.models.User
import org.json.JSONArray
import org.json.JSONObject


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFirstFragment()

        setUpRecyclerView()
        getUsers()
    }

    fun addFirstFragment()
    {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val tag = FirstFragment::class.java.name
        val addTransaction = transaction.add(
                R.id.first_fragment, FirstFragment.newInstance(), tag)
        addTransaction.commit()
    }

    fun setUpRecyclerView()
    {
        val recyclerView: RecyclerView = findViewById(R.id.list)
        val layoutManager: LinearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

        val users: ArrayList<User> = ArrayList<User>()
        users.add(User("Ciuca", "Alexandra"))
        users.add(User("Vlad", "Bogdan"))

        val adapter = MyAdapter(users, OnUserItemClick { user ->
            Toast.makeText(this, user.toString(), Toast.LENGTH_SHORT).show()
        })
        recyclerView.adapter = adapter
    }

    fun getUsers()
    {
        val volleyConfigSingleton = VolleyConfigSingleton.getInstance(this.applicationContext)
        val queue = volleyConfigSingleton.requestQueue
        val url = "https://jsonplaceholder.typicode.com/users"

        val getUsersRequest = StringRequest(
                Request.Method.GET,
                url,
                { responseJson ->
                    handleUsersResponse(responseJson)
                },
                { error ->
                    Toast.makeText(
                            this,
                            "ERROR: get users failed with error: ${error.message}",
                            Toast.LENGTH_SHORT
                    ).show()
                }
        )
        queue.add(getUsersRequest)
    }

    fun handleUsersResponse(response: String)
    {
        val usersJSONArray = JSONArray(response)

        for(index in 0 until usersJSONArray.length())
        {
            val userNameJSON: JSONObject? = usersJSONArray[index] as? JSONObject
            userNameJSON?.let {
                val name = userNameJSON.getLong("name")
            }
        }
    }


}

