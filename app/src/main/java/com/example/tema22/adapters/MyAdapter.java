package com.example.tema22.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.tema22.R;
import com.example.tema22.VolleyConfigSingleton;
import com.example.tema22.interfaces.OnUserItemClick;
import com.example.tema22.models.User;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.UserViewHolder> {

    ArrayList<User> users;
    OnUserItemClick onUserItemClick;

    public MyAdapter(ArrayList<User> users, OnUserItemClick onUserItemClick) {
        this.users = users;
        this.onUserItemClick = onUserItemClick;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_cell, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);

        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User user = users.get(position);

        holder.bind(user);
    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView surname;
        private View view;
        private ImageButton image;

        UserViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            surname = view.findViewById(R.id.surname);
            this.view = view;
            image = view.findViewById(R.id.image_button);
        }

        void bind(User user) {
            name.setText(user.getName());
            surname.setText(user.getSurname());
            String imageUrl = "D:/A.FACULTATE/an II/Android/Tema2.2/app/src/main/res/drawable/arrow.png";
            ImageLoader imageLoader = VolleyConfigSingleton.getInstance(image.getContext().getApplicationContext()).getImageLoader();
            imageLoader.get(imageUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    image.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onUserItemClick != null)
                    {
                        onUserItemClick.onClick(user);
                    }
                }
            });
        }
    }
}
