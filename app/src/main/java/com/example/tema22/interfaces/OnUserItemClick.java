package com.example.tema22.interfaces;

import com.example.tema22.models.User;

public interface OnUserItemClick {

    void onClick(User user);
}
